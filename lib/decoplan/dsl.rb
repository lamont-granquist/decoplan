require "decoplan/dive_profile"

module Decoplan
  module DSL
    def dive(name = nil, &block)
      DiveProfile.create_dive(name, &block)
    end

    def help
      puts "FIXME: should give the user some help"
    end

    def self.add_algorithm_to_dsl(symbol, klass)
      define_method symbol do |profile, *args|
        klass.new(profile, *args)
      end
    end
  end
end
