
require "decoplan/algorithm"

module Decoplan
  class DiveProfile < BasicObject
    attr_accessor :levels
    attr_accessor :name
    attr_accessor :deco_gases
    attr_accessor :current_gas

    Level = ::Struct.new(:depth, :time, :gas)

    class Gas
      attr_accessor :o2
      attr_accessor :he
      attr_accessor :mod

      def initialize(o2, he, mod: nil)
        @o2 = o2
        @he = he
        @mod = mod

        # normalize so we can use either 0.18, 0.45 or 18, 45
        if (@o2 + @he) > 1.0
          @o2 /= 100.0
          @he /= 100.0
        end
      end

      # FIXME: feels more like a utility function?  should mod be on the gas at all?
      def mod(ppo2)
        return @mod if @mod
        ( ppo2 / o2 - 1.0 ) * 33.0
      end

      def ==(o)
        o.class == self.class && o.state == state
      end

      alias_method :eql?, :==

      def hash
        state.hash
      end

      protected

      def state
        [@o2, @he]
      end
    end

    def initialize(name: nil)
      @name = name
      @levels = []
      @deco_gases = []
      @current_gas = nil
    end

    def deco_gas(o2, he = 0)
      deco_gases.push(Gas.new(o2, he))
    end

    def gas(o2, he = 0)
      self.current_gas = Gas.new(o2, he)
    end

    def level(depth, time, o2 = nil, he = 0)
      gas = o2 ? Gas.new(o2, he) : current_gas
      ::Kernel::raise "must have a current gas or set a gas on a level" if gas.nil?
      self.current_gas = gas
      levels.push(Level.new(depth, time, gas))
    end

    def apply(algorithm, *params)
      Algorithm.resolve(algorithm).new(self, *params)
    end

    def self.create_dive(name = nil, &block)
      dive = new(name: name)
      dive.instance_eval(&block) if block_given?
      dive
    end
  end
end
