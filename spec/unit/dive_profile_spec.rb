require "decoplan/dive_profile"

RSpec.describe Decoplan::DiveProfile do
  let(:name) { "test" }
  let(:profile) { described_class.new(name: name) }

  it "sets the name by default" do
    expect(profile.name).to eql "test"
  end

  it "the name is nil by default" do
    profile2 = described_class.new
    expect(profile2.name).to be nil
  end

  it "the name has an accessor" do
    profile.name = "changed"
    expect(profile.name).to eql("changed")
  end

  it "there are no levels by default" do
    expect(profile.levels).to eql([])
  end

  it "accepts a percentage gas command" do
    profile.gas(18, 45)
    expect(profile.current_gas.o2).to eql(0.18)
    expect(profile.current_gas.he).to eql(0.45)
  end

  it "accepts a fractional gas command" do
    profile.gas(0.18, 0.45)
    expect(profile.current_gas.o2).to eql(0.18)
    expect(profile.current_gas.he).to eql(0.45)
  end

  it "gases compare" do
    expect(Decoplan::DiveProfile::Gas.new(0.18, 0.45)).to eql(Decoplan::DiveProfile::Gas.new(0.18, 0.45))
    expect(Decoplan::DiveProfile::Gas.new(18, 45)).to eql(Decoplan::DiveProfile::Gas.new(0.18, 0.45))
  end

  it "levels compare" do
    gas = Decoplan::DiveProfile::Gas.new(0.18, 0.45)
    expect(Decoplan::DiveProfile::Level.new(100, 70, gas)).to eql(Decoplan::DiveProfile::Level.new(100, 70, gas))
  end

  it "accepts a level command" do
    profile.level(100, 70, 0.18, 0.45)
    expect(profile.levels).to eql([Decoplan::DiveProfile::Level.new(100, 70, Decoplan::DiveProfile::Gas.new(0.18, 0.45))])
  end

  it "a level command with no gas set throws an error" do
    expect { profile.level(100, 70) }.to raise_error(RuntimeError)
  end

  it "the current gas can be set before the level" do
    profile.gas(18, 45)
    profile.level(100, 70)
    expect(profile.levels).to eql([Decoplan::DiveProfile::Level.new(100, 70, Decoplan::DiveProfile::Gas.new(0.18, 0.45))])
  end

  it "setting a gas on the level sets the current gas" do
    profile.gas(32, 0)
    profile.level(100, 20, 18, 45)
    profile.level(120, 30)
    expect(profile.levels).to eql([
      Decoplan::DiveProfile::Level.new(100, 20, Decoplan::DiveProfile::Gas.new(0.18, 0.45)),
      Decoplan::DiveProfile::Level.new(120, 30, Decoplan::DiveProfile::Gas.new(0.18, 0.45)),
    ])
  end

  it "multiple levels can be set" do
    profile.gas(18, 45)
    profile.level(100, 20)
    profile.level(120, 30)
    profile.level(110, 10)
    expect(profile.levels).to eql([
      Decoplan::DiveProfile::Level.new(100, 20, Decoplan::DiveProfile::Gas.new(0.18, 0.45)),
      Decoplan::DiveProfile::Level.new(120, 30, Decoplan::DiveProfile::Gas.new(0.18, 0.45)),
      Decoplan::DiveProfile::Level.new(110, 10, Decoplan::DiveProfile::Gas.new(0.18, 0.45)),
    ])
  end

  it "multiple levels with different gas can be set" do
    profile.gas(30, 30)
    profile.level(100, 20)
    profile.gas(50, 0)
    profile.level(70, 3)
    profile.gas(100, 0)
    profile.level(20, 10)
    expect(profile.levels).to eql([
      Decoplan::DiveProfile::Level.new(100, 20, Decoplan::DiveProfile::Gas.new(0.30, 0.30)),
      Decoplan::DiveProfile::Level.new(70, 3, Decoplan::DiveProfile::Gas.new(0.50, 0.0)),
      Decoplan::DiveProfile::Level.new(20, 10, Decoplan::DiveProfile::Gas.new(1.00, 0.0)),
    ])
  end

  it "we can apply an algorithm" do
    klass = class_double(Decoplan::Algorithm::Buhlmann)
    instance = instance_double(Decoplan::Algorithm::Buhlmann)
    expect(Decoplan::Algorithm).to receive(:resolve).with(:buhlmann).and_return(klass)
    expect(klass).to receive(:new).with(profile).and_return(instance)
    expect(profile.apply(:buhlmann)).to eql(instance)
  end

  it "we can pass args to an algorithm" do
    klass = class_double(Decoplan::Algorithm::Buhlmann)
    instance = instance_double(Decoplan::Algorithm::Buhlmann)
    expect(Decoplan::Algorithm).to receive(:resolve).with(:buhlmann).and_return(klass)
    expect(klass).to receive(:new).with(profile, lo: 20, hi: 85).and_return(instance)
    expect(profile.apply(:buhlmann, lo: 20, hi: 85)).to eql(instance)
  end
end
